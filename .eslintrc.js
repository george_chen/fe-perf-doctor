/**
 * 参考文档
 * https://eslint.org/docs/user-guide/configuring
 * https://juejin.cn/post/7139077064748957732
 */

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2020
  },
  env: {
    browser: true,
    commonjs: true,
    node: true
  },
  extends: [
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // 忽略检测的全局变量
  globals: {
    // 视项目情况而定
  },
  plugins: [
    'node'
  ],
  // eslint 规则
  rules: {
    // allow debugger during development
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-alert': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 所有引号使用单引号
    quotes: ['error', 'single'],
    // 在 return、throw、continue 和 break 语句之后禁止无法访问的代码
    // 'no-unreachable': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 数组和对象键值对最后一个逗号， never参数：不能带末尾的逗号, always参数：必须带末尾的逗号，
    // always-multiline：多行模式必须带逗号，单行模式不能带逗号
    'comma-dangle': [1, 'never'],
    // 空格缩进 - 2个空格
    indent: ['error', 2, { SwitchCase: 1 }],
    // 强制数组方法的回调函数中有 return 语句
    'array-callback-return': 'error',
    // 强制generator函数*号的空格规则 - 前面没空格，后面有空格
    'generator-star-spacing': ['error', { before: false, after: true }],
    // 禁止多余的 return 语句
    'no-useless-return': 'error',
    // 分号是否需要
    semi: ['error', 'always'],
    // 在正则表达式的开头明确禁止除法运算符，默认关闭
    'no-div-regex': 'error',
    // 在 function 定义左括号之前强制保持一致的间距
    'space-before-function-paren': ['error', {
      anonymous: 'never', // function() {}
      named: 'never', // function foo() {}
      asyncArrow: 'always' // async () => {}
    }]
  }
};
