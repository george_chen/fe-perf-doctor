# fe-perf-doctor

## 介绍

前端项目源码性能检测大师，一款专为前端项目源码性能检测而设计的高级npm工具。此工具能够深度分析前端项目的源码，为您提供精准的性能评估和相应的优化改进建议，助您优化自己的前端项目，使其性能更上一层楼。

特点：

- 安装方便：只需通过npm全局安装，运行指令采用基于commander的智能方式，可轻松在终端中执行。
- 使用简单：通过运行相应的命令，工具将对前端项目进行全面检测，生成检测报告，让您更直观地看到自己的项目都存在哪些影响性能的问题点，从而可以有目的地进行相关的优化操作。

工具提供的能力：

- 前端项目源码性能检测
- 提供可配置的选项，允许用户自定义检测策略和规则 - TODO
- 多线程跑检测任务 - TODO
- 前端项目源码自动优化 - TODO
- React、Angular框架检测策略 - TODO

## 软件架构

性能检测架构图
[点击查看](https://gitee.com/george_chen/fe-perf-doctor/raw/master/static/inspect-arch-pic.png)

性能检测时序图
[点击查看](https://gitee.com/george_chen/fe-perf-doctor/raw/master/static/inspect-seq-pic.png)

tips: 如需更新上面的图片，可以按照以下步骤进行：

- 使用Visio打开static目录下的.vsdx文件或者使用StarUML打开static目录下的.mdj文件，更新内容并导出png。

- 分支合并到master。

## 安装教程

### 安装Node.js，需安装Node.js版本 >= 10.5.0

[参考教程](https://www.runoob.com/nodejs/nodejs-install-setup.html)

### npm在全局安装命令

```shell
npm install fe-perf-doctor -g
```

fe-perf-doctor将安装在全局node_modules目录下，bin目录下的指令会注册到全局指令。以下是几个常见操作系统的全局node_modules目录的默认位置：

- Windows: %AppData%\npm\node_modules(%AppData%是配在“我的电脑”-“高级系统设置”-“环境变量”-“Path”中的AppData变量)
- macOS: /usr/local/lib/node_modules
- Linux: /usr/lib/node_modules

## 使用说明

### 检测命令

```shell
# 示例
doctor inspect https://gitee.com/george_chen/fe-vue-demo.git rel_test1 --dirs_files=src^public --pic_limit=300
```

#### 命令选项

| 选项名称     | 描述                      | 取值          |
|-------------|---------------------------|---------------|
| --dirs_files | 指定需要检测的前端项目下的一级目录或者一级文件，存在多个使用^隔开，默认src目录 | src or src^public |
| --pic_limit | 图片限值，超过该则会进行压缩 | 正数，单位KB |
| --debug     | 开发debug模式，日常无需使用，默认false | true or false |

## 参与贡献

### 拉取分支规范

1. Fork 本仓库
2. 新建 rel_xxx 分支
3. 提交代码
4. 新建 Pull Request
5. 使用 README_XXX.md 来支持不同的语言，默认中文无需设置。例如 README_en.md

### 本地开发

#### 安装npm依赖

```shell
npm install
```

#### 执行bin/doctor.js文件

```shell
# windows系统-示例
node .\bin\doctor.js inspect https://gitee.com/george_chen/fe-vue-demo.git rel_test1 --dirs_files=src^public --pic_limit=300 --debug=true
```

### 发布NPM

- 在命令行界面中，进入到包含您的npm包代码的项目文件夹，并运行以下命令来登录到npm账号：

```shell
npm login
```

输入您的npm账号凭据（用户名、密码和电子邮件），以登录到npm账号。

- 修改package.json文件中version。

- 在命令行界面中，运行以下命令来发布npm包：

```shell
npm publish
```

## 其他

1. 以下是给Gitee打广告：
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
