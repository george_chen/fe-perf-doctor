#!/usr/bin/env node
// 上面那行命令主要是为了让系统看到时会沿着该路径去查找node并执行，主要是为了兼容Mac，确保可执行。
// 启用module-alias库以解析别名路径
require('module-alias/register');
const { version, dependencies } = require('../package.json');
const program = require('commander');
const inspectExe = require('@lib/inspectExe');

// 自定义options - version
function logVersion(version) {
  console.log(version);
  process.exit(0);
}
program.option('-V, -v, --version', '查看版本号', () => {
  logVersion(version);
});
// 获取命令行参数中是否包含 --version 选项
const hasVersionOption = process.argv.includes('--version');
// 如果包含 --version 选项，则输出版本号并退出程序
if (hasVersionOption) {
  logVersion(version);
}

/* 核心命令-开始 */
program
  .command('inspect <prjGitUrl> <branchName>')
  .option('--dirs_files <dirsFiles>', '指定需要检测的前端项目下的一级目录或者一级文件，存在多个使用逗号,隔开，默认src目录')
  .option('--pic_limit <picLimit>', '图片限值，超过该则会进行压缩，单位KB的正数')
  .option('--debug <debug>', '开发debug模式，日常无需使用，可选值：true/false，默认false')
  .description('检测某个git仓库某个分支下的前端项目')
  .action((prjGitUrl, branchName, cmd) => {
    // 执行检测
    inspectExe({
      prjGitUrl,
      branchName,
      picLimit: cmd.pic_limit,
      debug: cmd.debug === 'true',
      dirsFiles: cmd.dirs_files,
      pkgJsonDep: dependencies
    });
  });
// program
//   .command('optimize <prjDir>')
//   .description('优化某个目录下的前端项目')
//   .action((prjDir) => {
//     console.log(prjDir);
//   });
/* 核心命令-结束 */

// 自定义options - help
// 禁用默认options里的帮助命令
program.helpOption(false);
// 禁用默认command里的帮助命令
program.addHelpCommand(false);
function logHelp() {
  console.log('Usage: \n  doctor [option] [command]');
  // 打印选项信息
  console.log('Options:');
  program.options.forEach((option) => {
    console.log(`  ${option.flags}: ${option.description}`);
  });
  // 打印命令信息
  console.log('Commands:');
  program.commands.forEach((command) => {
    const commandName = command.name();
    const commandArgs = command._args.map((arg) => `<${arg._name}>`).join(' ');
    console.log(`  ${commandName} ${commandArgs}: ${command.description()}`);
  });
  process.exit(0);
}
program.option('-H, -h, --help', '查看帮助信息', () => {
  logHelp();
});
// 获取命令行参数中是否包含 --help 选项
const hasHelpOption = process.argv.includes('--help');
// 如果包含 --help 选项，则查看帮助信息并退出程序
if (hasHelpOption) {
  logHelp();
}

program.parse();
