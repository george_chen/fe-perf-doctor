/* 常量文件 */
module.exports = {
  // 默认存放项目的目录
  DEFAULT_PRJ_DIR: 'demo',
  // 报告输出目录
  REPORT_OUTPUT_DIR: 'reports',
  // 报告临时文件名
  REPORT_TEMP_FILENAME: 'temp.json',
  // 报告文件名
  REPORT_FINAL_FILENAME: 'report.html',
  // 图片默认阈值100KB，过大则需要压缩
  DEFAULT_PICTURE_LIMIT: 100,
  // 默认的指定需要检测的前端项目下的一级目录或者一级文件
  DEFAULT_DIRS_FILES: ['src'],
  // 前端项目常用文件后缀名
  FE_FILES_SUFFIX: ['.html', '.css', '.scss', '.sass', '.less', '.styl', '.stylus', '.js', '.mjs', '.ts'],
  // 前端常用静态资源对应扩展名映射
  STATIC_RESOURCES_EXTNAME_MAP: {
    img: ['.jpeg', '.jpg', '.png', '.gif', '.svg', '.webp', '.bmp', '.tiff'],
    html: ['.html'],
    css: ['.css', '.scss', '.sass', '.less', '.styl', '.stylus'],
    js: ['.js', '.mjs'],
    ts: ['.ts'],
    json: ['.json'],
    vue: ['.vue'],
    react: ['.jsx', '.tsx']
  },
  // 模板html文件内容
  TPL_HTML_CONTENT: `
    <html lang="zh">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title><!-- TITLE_PLACEHOLDER --></title>
      <style>
        h1 {
          text-align: center;
        }
      </style>
    </head>
    <body>
      <!-- CONTENT_PLACEHOLDER -->
    </body>
    </html>
  `,
  // 格式化报告文件键名映射中文名
  FORMAT_REPORT_DATA_MAP: {
    fileName: '文件名称',
    size: '文件大小',
    filePath: '文件路径',
    ext: '扩展名',
    warnMsg: '警告信息',
    errType: '错误类型',
    errMsg: '错误信息',
    lineNo: '行号',
    columnNo: '列号'
  },
  // DOM 嵌套最大深度阈值
  DOM_DEPTH_LIMIT: 10,
  // 所有可以直接存放文本的常见HTML标签
  STORE_TEXT_TAGS: ['div', 'p', 'span', 'a', 'strong', 'em', 'b', 'i', 'u', 's', 'mark', 'q', 'cite', 'code', 'pre'],
  // 所有可以自闭合（即不需要闭合标签）的标签
  SELF_CLOSE_TAGS: ['br', 'hr', 'img', 'input', 'link', 'meta', 'col'],
  // 最大CSS样式层级数阈值
  MAX_CSS_HIERARCHY_LEVEL: 4,
  // 非css文件转化成css文件对应的方法映射
  NONCSS_2_CSS_MAP: {
    '.less': 'less2Css',
    '.sass': 'sass2Css',
    '.styl': 'stylus2Css',
    '.stylus': 'stylus2Css'
  },
  // 昂贵css样式属性和属性值
  EXPENSIVE_CSS_PROPS_VALUES: ['box-shadow', 'text-shadow', 'border-radius', 'filter', 'transform', 'transition', 'animation', 'perspective', 'transform-style', 'text-indent', 'opacity'],
  // 事件监听器使用阈值
  MAX_EVENT_LISTENER: 10,
  // Canvas、SVG 或 Web Animation API 的相关标识符(转换成小写)
  ANIMATION_APIS: ['canvas', 'canvasrenderingcontext2d', 'svgelement', 'animation'],
  // TypeScript类型 定义名称字符长度阈值
  MAX_TYPESCRIPT_NAME_LENGTH: 30,
  // TypeScript类型 最大允许的嵌套深度阈值
  MAX_TYPESCRIPT_NESTING_DEPTH: 5,
  // TypeScript类型 最大允许的类型复杂性阈值
  MAX_TYPESCRIPT_TYPE_COMPLEXITY: 20,
  // vue文件大小默认阈值20KB，过大则需要给出警告
  DEFAULT_VUE_FILE_SIZE_LIMIT: 20,
  // vue eslint规则
  VUE_ESLINT_RULES: [
    // 规则1.同一元素是否同时使用 v-if 和 v-for
    {
      ruleId: 'vue/no-use-v-if-with-v-for',
      ruleVal: 'error',
      ruleMsg: '同一元素同时使用v-if和v-for，建议分开使用'
    },
    // 规则2.在v-for循环中是否使用v-bind:key
    {
      ruleId: 'vue/require-v-for-key',
      ruleVal: 'error',
      ruleMsg: '在v-for循环中没有使用key属性，建议使用key属性'
    },
    // 规则3.是否在模板中使用v-bind:key
    {
      ruleId: 'vue/no-template-key',
      ruleVal: 'error',
      ruleMsg: '在<template>标签上使用key属性，建议不使用key属性'
    },
    // 规则4.是否存在定义但未使用的变量
    {
      ruleId: 'no-unused-vars',
      ruleVal: 'error',
      ruleMsg: '存在定义但未使用的变量，建议删除'
    },
    // 规则5.是否直接使用v-html
    {
      ruleId: 'vue/no-v-html',
      ruleVal: 'error',
      ruleMsg: '直接使用v-html，会存在安全隐患，建议使用其他方式代替'
    },
    // 规则6.计算属性中是否使用具有副作用的函数或方法
    {
      ruleId: 'vue/no-side-effects-in-computed-properties',
      ruleVal: 'error',
      ruleMsg: '计算属性中使用具有副作用的函数或方法，建议使用其他方式代替'
    }
    // 继续新增其他规则...
  ],
  // vue eslint规则配置
  VUE_ESLINTRC_CONFIG: {
    overrideConfig: {
      parser: 'vue-eslint-parser',
      parserOptions: {
        sourceType: 'module'
      },
      extends: [
        'plugin:vue/vue3-recommended', // 使用 Vue 3 推荐的 ESLint 规则vue/vue3-recommended
        'plugin:vue/essential' // 使用 Vue 2 推荐的 ESLint 规则vue/essential
      ],
      rules: {}
    }
  }
};
