/* React框架-特定文件检测策略 */
const {
  isValuableArray
} = require('@lib/utils/tools');
const CONSTS = require('@lib/consts/index');
const Report = require('@lib/utils/report');

// 需要写入报告文件的数据
const outputList = [];

class FwReact {
  /**
   * 执行策略的函数
   * @param {*} reactList React框架特定文件列表
   * @param {*} extra 额外参数
   * @param {*} cb 回调函数
   */
  static execute(reactList = [], extra, cb) {
    try {
      if (!isValuableArray(reactList)) {
        cb && cb();
        return;
      }
      // 1.策略开始
      FwReact.testReact(reactList, extra, outputList);
      // 2.输出数据到报告目录下的临时文件
      if (!outputList.length) {
        cb && cb();
        return;
      }
      const { index } = extra;
      Report.outputTempFile(
        outputList,
        'React框架文件',
        `${index}fw-react-${CONSTS.REPORT_TEMP_FILENAME}`,
        () => {
          cb && cb();
        }
      );
    } catch (e) {
      console.error(`执行React框架-特定文件检测策略报错：${e}`);
      cb && cb();
    }
  }

  static testReact(reactList, extra, outputList = []) {
    // TODO
    console.log('reactList', reactList);
  }
}
module.exports = FwReact;
