/* 静态资源-图片 检测策略 */
const {
  isValuableArray
} = require('@lib/utils/tools');
const CONSTS = require('@lib/consts/index');
const Report = require('@lib/utils/report');

// 需要写入报告文件的数据
const outputList = [];

class SrImg {
  /**
   * 执行策略的函数
   * @param {*} imgList 图片列表
   * @param {*} extra 额外参数
   * @param {*} cb 回调函数
   */
  static execute(imgList = [], extra, cb) {
    if (!isValuableArray(imgList)) {
      cb && cb();
      return;
    }
    // 1.策略开始
    SrImg.detectExcessiveImg(imgList, extra, outputList);
    // 2.输出数据到报告目录下的临时文件
    if (!outputList.length) {
      cb && cb();
      return;
    }
    const { index } = extra;
    Report.outputTempFile(
      outputList,
      '静态资源-图片',
      `${index}sr-img-${CONSTS.REPORT_TEMP_FILENAME}`,
      () => {
        cb && cb();
      }
    );
  }

  /**
   * 策略1：图片资源过大检测-图片资源过大在弱网情况下会导致接口阻塞
   * @param {*} imgList 图片列表
   * @param {*} extra 额外参数
   * @param {*} outputList 报告文件的数据列表
   */
  static detectExcessiveImg(imgList, extra, outputList = []) {
    const { picLimit } = extra;
    const targets = imgList.filter(img => img.size >= picLimit);
    isValuableArray(targets) && Report.formatAndpushReportData(
      targets.map(item => ({
        fileName: item.fileName,
        size: `${item.size}KB`,
        filePath: item.filePath
      })),
      '图片资源过大',
      outputList
    );
  }
}
module.exports = SrImg;
