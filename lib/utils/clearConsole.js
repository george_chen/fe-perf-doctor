/* 清空控制台 */
const readline = require('readline');

module.exports = function clearConsole(tips) {
  // Node.js 是否在终端上下文中运行
  if (process.stdout.isTTY) {
    // 根据当前终端窗口的行数重复换行字符
    // 比如：如果你的终端窗口有10行，'\n'.repeat(process.stdout.rows)，
    // 将生成一个包含10个换行字符的字符串 (\n\n\n\n\n\n\n\n\n\n)
    const blank = '\n'.repeat(process.stdout.rows);
    console.log(blank);
    // 将光标定位到终端窗口的左上角（行数0，列数0），以便从该位置开始输出内容或执行其他操作
    readline.cursorTo(process.stdout, 0, 0);
    // 清除终端屏幕上从当前光标位置到屏幕底部的所有内容，使屏幕上方的内容保持可见
    readline.clearScreenDown(process.stdout);
    if (tips) {
      console.log(tips);
    }
  }
};
