/* js操作 */
const parser = require('@babel/parser');
const CONSTS = require('@lib/consts/index');

class Js {
  /**
   * 根据js代码获取AST
   * @param {*} code js代码内容
   * @returns AST抽象语法树
   */
  static getAstByJsCode(code) {
    const ast = parser.parse(code, {
      sourceType: 'module', // 'module' 表示 ECMAScript 模块，'script' 表示脚本
      plugins: [
        'jsx', // 代码中包含 JSX
        'typescript' // 如果代码中包含 TypeScript
      ]
    });
    return ast;
  }

  /**
   * 判断是否是否使用了 eval 函数
   * @param {*} path 遍历AST抽象语法树获取的当前节点路径
   * @returns 布尔值，是否使用了 eval 函数
   */
  static isEvalCall(path) {
    return path.node.callee.name === 'eval';
  }

  /**
   * 判断是否是 JavaScript 绘制动画相关的 API
   * @param {*} path 遍历AST抽象语法树获取的当前节点路径
   * @returns 布尔值，是否是 JavaScript 绘制动画相关的 API
   */
  static isAnimationAPIs(path) {
    return CONSTS.ANIMATION_APIS.includes(path.node.name.toLowerCase());
  }
}

module.exports = Js;
